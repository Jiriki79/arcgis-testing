
let remoteInterface;

/** ID of connected iSpy instance. This can be expanded to an array of iSpy instances */
var ispy_group
export let instanceID = '';

/** Array of events received from iSpy */
const events = [];

export var ispy_user
export var ispy_group
export var securityLabel
export var image_id
export var shapes


document.addEventListener('DOMContentLoaded', () => {
    // Option 1, Global
    remoteInterface = iSpy.RemoteInterface;
    remoteInterface.__promise_timeout_ms = 1000 * 60 * 5; 
    //updateVersion(remoteInterface.__version);
    getUser().then(() => setupRemote());

    // Option 2, UMD
    // require(['ispy-remote-interface.js'], iSpy => {
    //     remoteInterface = iSpy.RemoteInterface;
    //     updateVersion(remoteInterface.__version);
    //     getUser().then(() => setupRemote());
    // });

    //setupImages();
});

// Connect and setup remote callbacks
function setupRemote() {
    let group = new URLSearchParams(location.search).get("remote_group")
    updateGroup(group);

    // Connect to Socket Server
    remoteInterface.connect(CONFIG.messageBroker, 'FALLBACK', {pollingFrequency: 4000, group }).then(() => {
        // Request status from any existing iSpy instances, which will send a message with their image & extents
        remoteInterface.requestStatus().then(p => console.log('Request Status Promise', p));
        updateEvent('Requesting status from existing instances...');

        // Setup Callbacks for messages from iSpy
        remoteInterface.onStatus(statusPayload => {
            updateInstanceID(statusPayload.instanceID);
            //updateInstanceVersion(statusPayload.iSpyVersion);
            updateImage(
                statusPayload.imageID,
                statusPayload.imageGuide,
                statusPayload.imagePath,
                statusPayload.imageSecurity,
                statusPayload.imageDate,
                statusPayload.imageExtents,
                imagePayload.shapes.geometry.coordinates
            );
            updateViewport(statusPayload.viewportExtents);
            updateEvent(`Received Instance Status: ${statusPayload.instanceID}`);
            logPayload(statusPayload);
        });

        remoteInterface.onConnection(connectionPayload => {
            updateInstanceID(connectionPayload.instanceID);
            //updateInstanceVersion(connectionPayload.iSpyVersion);
            updateEvent(`Instance Connected: ${connectionPayload.instanceID}`);
            logPayload(connectionPayload);
        });

        remoteInterface.onImageLoad(imagePayload => {
            updateEvent(`Image Loaded: ${imagePayload.imageID}`);
            logPayload(imagePayload);
            updateImage(
                imagePayload.imageID,
                imagePayload.imageGuide,
                imagePayload.imagePath,
                imagePayload.imageSecurity,
                imagePayload.imageDate,
                imagePayload.imageExtents,
                imagePayload.shapes.geometry.coordinates
            );
        });
        remoteInterface.onImageClicked(clickPayload => {
            updateEvent(`Image Clicked: ${clickPayload.line}, ${clickPayload.sample}`)
            logPayload(clickPayload);
        })

        remoteInterface.onViewportChanged(extentsPayload => {
            updateInstanceID(extentsPayload.instanceID);
            updateViewport(extentsPayload.viewportExtents);
            updateEvent('Viewport Changed');
            logPayload(extentsPayload);
        });

        remoteInterface.onLayerCreated(FeatureConfigPayload => {
            updateEvent(`Layer Created: ${FeatureConfigPayload.config.layerName}`)
            logPayload(FeatureConfigPayload);
        });

        /**
        * ispy.RemoteInterface calls the onObservationSaved function
        * this is where the observation should be saved or sent to a database
        */
        remoteInterface.onObservationSaved(payload => {
            updateEvent(`Observation Received: ${JSON.stringify(payload.message)}`);
            logPayload(payload);
        });

        remoteInterface.onShapeCreated(shapePayload => {
            // 2.1.7
            if(!shapePayload.shapes) {
                updateEvent(`Shape Created: ${shapePayload.shape.shapeID}`);    
                logPayload(shapePayload);
                return;
            }
            // 2.1.8
            const shapes = shapePayload.shapes.map(shape => [shape.shapeID, shape.shapeType]);
            updateEvent(`Shape Created: ${shapes}`);
            logPayload(shapePayload);
        });

        remoteInterface.onShapeUpdated(updatePayload => {
            const shapes = updatePayload.updates.map(update => update.shapeID);
            updateEvent(`Shape Updated: ${shapes}`);
            logPayload(updatePayload)
        })

        remoteInterface.onShapeDeleted(shapePayload => {
            // 2.1.7
            if(!shapePayload.shapeIDs) {
                updateEvent(`Shape Deleted: ${shapePayload.shapeID}`);    
                logPayload(shapePayload)
                return;
            }
            // 2.1.8
            updateEvent(`Shape Deleted: ${shapePayload.shapeIDs}`);
            logPayload(shapePayload)
        });

        remoteInterface.onShapeSelected(shapeSelectedPayload => {
            updateEvent(`Shape Selected: ${shapeSelectedPayload.shapeIDs}`);
            logPayload(shapeSelectedPayload)
        });

        remoteInterface.onDisconnection(disconnectPayload => {
            if (instanceID && disconnectPayload.instanceID === instanceID) {
                updateInstanceID('');
                updateEvent(`Instance Disconnected: ${disconnectPayload.instanceID}`);
                logPayload(disconnectPayload)
            }
        });

        remoteInterface.onSystemMessage((messagePayload) => window.alert(`System Broadcast\n${messagePayload.message}`))
        remoteInterface.onSystemDisconnect(() => {
            updateEvent('Disconnected by server');
        })
    });
}

// Action Functions //
/*function centerViewport() {
    updateEvent('Centering viewport...');
    const lat = document.getElementById('center_lat').valueAsNumber;
    const lon = document.getElementById('center_lon').valueAsNumber;
    const scale = document.getElementById('center_scale').valueAsNumber;

    remoteInterface.setViewportCenter({
        instanceID: instanceID,
        scale: scale,
        groundCoordinate: {
            lat: lat,
            lon: lon
        }
    }).then(p => {
        updateEvent('Centered viewport.');
        logPayload(p);
        console.log('Center Viewport Promise', p)
    });
}*/

document.getElementById('viewport').addEventListener('click',viewportImage)

function viewportImage() {
    updateEvent('Requesting viewport image...')
    remoteInterface.requestViewportImage().then(p => {
        updateEvent('Received image: ' + p.url);
        logPayload(p);
        console.log('Request Viewport Image Promise', p);
        window.open(p.url);
    })
}

function drawShape() {
    updateEvent('Creating shapes...');
    const shapeID = document.getElementById('draw_shape_id').value;
    const verticesStr = document.getElementById('vertices').value;
    const shapeType = document.getElementById('shape_type').value;

    remoteInterface.createShapes({
        instanceID: instanceID,
        shapes: [
            {
                type: 'Feature',
                id: shapeID,
                properties: {
                    color: 'yellow',
                    metadata: {
                        title: shapeID,
                        date: Date.now(),
                        description: '',
                        customMetadata: { a: 1, b: 'test', c: { x: 123, y: false } }
                    },
                    allowEditing: false
                },
                geometry: {
                    type: getType(shapeType),
                    coordinates: getVertices(verticesStr, shapeType)
                }
            }
        ],
        select: true
    }).then(p => {
        updateEvent('Created shapes.');
        logPayload(p);
        console.log('Create Shapes Promise', p)
    }).catch(e => {
        updateEvent(`Error creating shapes: ${e}`)
    })
}

function drawBBox() {
    const ul_s = document.getElementById('extents_ul').value.split(',');
    const ur_s = document.getElementById('extents_ur').value.split(',');
    const ll_s = document.getElementById('extents_ll').value.split(',');
    const lr_s = document.getElementById('extents_lr').value.split(',');
    remoteInterface.createShapes({
        instanceID: instanceID,
        shapes: [
            {
                type: 'Feature',
                id: 'BBOX',
                properties: {
                    color: 'yellow'
                },
                geometry: {
                    type: 'Polygon',
                    coordinates: [[[ul_s[1], ul_s[0]], [ur_s[1], ur_s[0]], [lr_s[1], lr_s[0]], [ll_s[1], ll_s[0]]]]
                }
            }
        ]
    });
}

function drawGeoJSON() {
    try {
        const geoJson = JSON.parse(document.getElementById('draw_geojson_textarea').value) || {};
        let shapes = [];
        if (geoJson.type === 'FeatureCollection') {
            shapes = geoJson.features || [];
        }
        else if (geoJson.type === 'Feature') {
            shapes = geoJson.geometry ? [geoJson] : [];
        } else {
            switch (geoJson.type) {
                case 'Point':
                case 'LineString':
                case 'Polygon':
                case 'MultiPoint':
                case 'MultiLineString':
                case 'MultiPolygon':
                case 'GeometryCollection': {
                    shapes = [{
                        type: 'Feature',
                        geometry: geoJson
                    }]
                    break;
                }
            }
        }
        remoteInterface.createShapes({
            instanceID: instanceID,
            shapes: shapes,
            select: true
        }).then(p => {
            updateEvent('Created shapes.');
            logPayload(p);
            console.log('Create Shapes Promise', p)
        }).catch(e => {
            updateEvent(`Error creating shapes: ${e}`)
        });
    } catch (e) {
        updateEvent(`Error drawing GeoJSON: ${e}`)
    }
}

function deleteShape() {
    updateEvent('Deleting shapes...');
    const shapeIDs = document.getElementById('delete_shape_id').value;

    remoteInterface.deleteShapes({
        instanceID: instanceID,
        shapeIDs: shapeIDs ? shapeIDs.split(',') : ''
    }).then(p => {
        updateEvent('Deleted shapes.');
        logPayload(p);
        console.log('Delete Shapes Promise', p)
    }).catch(e => {
        updateEvent(`Error deleting shapes: ${e}`)
    })
}

function setupImages() {
    // When a user clicks & drags an HTML object, the object is anchored to where the user clicked.
    // This can be any coordinate on an image element, which cannot be retrieved by iSpy.
    // In order to make the marker iSpy creates align with the user's drag coordinate,
    // forcibly align the drag coordinate to the center of the image.
    const image = document.getElementById('drag_image');
    image.addEventListener('dragstart', e => alignImage(e));
}

document.getElementById('getShape').addEventListener('click',getShapes)

window.getShapes = getShapes

function getShapes() {
    updateEvent('Getting shapes...');
    remoteInterface
        .getShapes({
            instanceID: instanceID
        })
        .then(r => {
            updateEvent('Retrieved Shapes');
            logPayload(r);
            console.log('Retrieved Shapes: ', r);
            shapes = r
        });
}

document.getElementById('connect').addEventListener('click',autoConnect)

function autoConnect() {
    updateEvent('Demonstrating auto connect... (check example.js::autoConnect)');
    remoteInterface.disconnect(); // In case we are already connected
    remoteInterface.autoConnect(CONFIG.messageBroker, 'WEBSOCKETS').then(statusPayload => {
        updateInstanceID(statusPayload.instanceID);
        //updateInstanceVersion(statusPayload.iSpyVersion);
        updateImage(
            statusPayload.imageID,
            statusPayload.imageSecurity,
            statusPayload.imageDate,
            statusPayload.imageExtents,
            imagePayload.shapes.geometry.coordinates
        );
        updateViewport(statusPayload.viewportExtents);
        updateEvent(`Auto Connected: ${statusPayload.instanceID}`);
        logPayload(statusPayload);
    })
}

document.getElementById('disconnect').addEventListener('click',disconnect)

function disconnect() {
    updateEvent('Disconnected');
    remoteInterface.disconnect();
}

// Helper Functions //
function getType(shapeType) {
    switch (shapeType) {
        case 'polyline':
            return 'LineString';
        case 'polygon':
            return 'Polygon';
        case 'marker':
        default:
            return 'Point';
    }
}

function getVertices(verticesStr, shapeType) {
    const type = getType(shapeType);

    const parsed = verticesStr
        .replace(/\(/g, '')
        .replace(/\)/g, '')
        .split(',');
    if (parsed.length < 2 || parsed.length % 2 !== 0) {
        return [];
    }
    const vertices = [];
    for (let i = 0; i < parsed.length; i += 2) {
        vertices.push([parseFloat(parsed[i + 1]), parseFloat(parsed[i])]);
    }

    switch (type) {
        case 'Point':
            return vertices[0];
        case 'LineString':
            return vertices;
        case 'Polygon':
            return [vertices]
    }
}

function updateUser(user) {
    document.getElementById('connected_user_info').innerText = user;
    ispy_user = user
}

function updateGroup(group) {
    if(group) {
        ispy_group = group
        document.getElementById('group_info').innerText = group;
    } else {
        document.getElementById('group_info').innerText = "Set group using ?remote_group query parameter";
    }
}


function updateVersion(version) {
    document.getElementById('version').innerText = version;
}

function updateInstanceID(id) {
    if (!instanceID || id !== instanceID) {
        instanceID = id;
        document.getElementById('connected_instance').innerText = instanceID;
    }
}

function updateInstanceVersion(version) {
    document.getElementById('instance_version').innerText = version;
}

function updateImage(id, guide, imagePath, security, dateMS, imageExtents, extra) {
    if(id) {
        imageID = id
        document.getElementById('image_id').innerText = id;
    }
    if(extra){
        shapes = extra
        document.getElementById('shapes').innerText = extra;
    }
    if(guide) {
        document.getElementById('image_guide').innerText = guide;
    }
    if(imagePath) {
        document.getElementById('image_path').innerText = imagePath;
    }
    if(security) {
        securityLabel = [security.classification, security.sciControl, security.dissemination].filter(label => !!label).join('//');
        document.getElementById('image_classification').innerText = securityLabel;
    }
    if(dateMS) {
        document.getElementById('image_date').innerText = new Date(dateMS);
    }
    if(imageExtents) {
        const image_extents_ul = document.getElementById('image_extents_ul');
        const image_extents_ur = document.getElementById('image_extents_ur');
        const image_extents_ll = document.getElementById('image_extents_ll');
        const image_extents_lr = document.getElementById('image_extents_lr');
        image_extents_ul.value = formatLatLon(imageExtents.upperLeft);
        image_extents_ur.value = formatLatLon(imageExtents.upperRight);
        image_extents_ll.value = formatLatLon(imageExtents.bottomLeft);
        image_extents_lr.value = formatLatLon(imageExtents.bottomRight);
    }
}

function updateViewport(groundExtents) {
    if (!groundExtents) {
        return;
    }
    const viewport_extents_ul = document.getElementById('viewport_extents_ul');
    const viewport_extents_ur = document.getElementById('viewport_extents_ur');
    const viewport_extents_ll = document.getElementById('viewport_extents_ll');
    const viewport_extents_lr = document.getElementById('viewport_extents_lr');
    viewport_extents_ul.value = formatLatLon(groundExtents.upperLeft);
    viewport_extents_ur.value = formatLatLon(groundExtents.upperRight);
    viewport_extents_ll.value = formatLatLon(groundExtents.bottomLeft);
    viewport_extents_lr.value = formatLatLon(groundExtents.bottomRight);
}

function updateEvent(text) {
    const d = new Date();
    events.push(`[${d.toLocaleTimeString('en-US', { hour12: false })}] ${text}`);
    const textArea = document.getElementById('iSpyOutput');
    textArea.value = events
        .slice()
        .reverse()
        .join('\n');
}

function getUser() {
    // Get GeoAxis User in order to connect to User's iSpy
    updateEvent('Getting user...');
    return fetch(CONFIG.auth)
        .then(r => r.json())
        .then(auth => updateUser(parseUser(auth)))
        .catch(e => updateUser('No User'));
}

function parseUser(auth) {
    if (!auth) {
        return 'No User';
    }
    const userString = auth.dn;
    const cn = userString.indexOf('cn=');
    const com = userString.indexOf(',');
    if (cn === -1 || com === -1) {
        return 'No User';
    }
    const parsed = userString.slice(cn + 3, com);
    return parsed;
}

function formatLatLon(gc) {
    return `${gc.lat.toFixed(3)}, ${gc.lon.toFixed(3)}`;
}

function formatLineSample(ic) {
    return `${ic.line}, ${ic.sample}`;
}

function alignImage(e) {
    const img = document.getElementById('drag_image');
    e.dataTransfer.setDragImage(img, img.width / 2, img.height / 2);
}

function logPayload(p) {
    const textarea = document.getElementById('iSpyOutput');
    textarea.value = JSON.stringify(p, null, '\t');
    
}