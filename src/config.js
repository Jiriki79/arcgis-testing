var hostAddress = 'https://cgswebdev.com'
export var CONFIG = {
    messageBroker: hostAddress + '/cgswebPubSubBroker',
    auth: hostAddress + '/auth/'
}
