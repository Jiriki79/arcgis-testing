import {CONFIG} from './config'
let remoteInterface;

/** ID of connected iSpy instance. This can be expanded to an array of iSpy instances */
var ispy_group
export let instanceID = '';

/** Array of events received from iSpy */
const events = [];

export var ispy_user
export var ispy_group
export var securityLabel
export var image_id
export var shapes

document.addEventListener('DOMContentLoaded', () => {
    // Option 1, Global
    remoteInterface = iSpy.RemoteInterface;
    remoteInterface.__promise_timeout_ms = 1000 * 60 * 5; 
    //updateVersion(remoteInterface.__version);
    getUser().then(() => setupRemote());

    // Option 2, UMD
    // require(['ispy-remote-interface.js'], iSpy => {
    //     remoteInterface = iSpy.RemoteInterface;
    //     updateVersion(remoteInterface.__version);
    //     getUser().then(() => setupRemote());
    // });

    //setupImages();
});

// Connect and setup remote callbacks
function setupimageIDimageRemote() {
    let group = new URLSearchParams(location.search).get("remote_group")
    updateGroup(group);

    // Connect to Socket Server
    remoteInterface.connect(CONFIG.messageBroker, 'FALLBACK', {pollingFrequency: 4000, group }).then(() => {
        // Request status from any existing iSpy instances, which will send a message with their image & extents
        remoteInterface.requestStatus().then(p => console.log('Request Status Promise', p));
        updateEvent('Requesting status from existing instances...');

        // Setup Callbacks for messages from iSpy
        remoteInterface.onStatus(statusPayload => {
            updateInstanceID(statusPayload.instanceID);
            //updateInstanceVersion(statusPayload.iSpyVersion);
            image_id=statusPayload.imageID;
            updateImage(
                statusPayload.imageID,
                statusPayload.imageGuide,
                statusPayload.imageSecurity,
                statusPayload.imageExtents
            );
            updateViewport(statusPayload.viewportExtents);
            updateEvent(`Received Instance Status: ${statusPayload.instanceID}`);
            logPayload(statusPayload);
        });

        remoteInterface.onConnection(connectionPayload => {
            updateInstanceID(connectionPayload.instanceID);
            //updateInstanceVersion(connectionPayload.iSpyVersion);
            updateEvent(`Instance Connected: ${connectionPayload.instanceID}`);
            logPayload(connectionPayload);
        });

        remoteInterface.onImageLoad(imagePayload => {
            updateEvent(`Image Loaded: ${imagePayload.imageID}`);
            logPayload(imagePayload);
            updateImage(
                imagePayload.imageID,
                imagePayload.imageGuide,
                imagePayload.imageSecurity,
                imagePayload.imageExtents,
                imagePayload.shapes.geometry.coordinates
            );
        });
        remoteInterface.onImageClicked(clickPayload => {
            updateEvent(`Image Clicked: ${clickPayload.line}, ${clickPayload.sample}`)
            logPayload(clickPayload);
        })

        remoteInterface.onViewportChanged(extentsPayload => {
            updateInstanceID(extentsPayload.instanceID);
            updateViewport(extentsPayload.viewportExtents);
            updateEvent('Viewport Changed');
            logPayload(extentsPayload);
        });

        remoteInterface.onLayerCreated(FeatureConfigPayload => {
            updateEvent(`Layer Created: ${FeatureConfigPayload.config.layerName}`)
            logPayload(FeatureConfigPayload);
        });

        /**
        * ispy.RemoteInterface calls the onObservationSaved function
        * this is where the observation should be saved or sent to a database
        */
        remoteInterface.onObservationSaved(payload => {
            updateEvent(`Observation Received: ${JSON.stringify(payload.message)}`);
            logPayload(payload);
        });

        remoteInterface.onShapeCreated(shapePayload => {
            // 2.1.7
            if(!shapePayload.shapes) {
                updateEvent(`Shape Created: ${shapePayload.shape.shapeID}`);    
                logPayload(shapePayload);
                return;
            }
            // 2.1.8
            const shapes = shapePayload.shapes.map(shape => [shape.shapeID, shape.shapeType]);
            updateEvent(`Shape Created: ${shapes}`);
            logPayload(shapePayload);
            window.shapes=shapePayload
        });

        remoteInterface.onShapeUpdated(updatePayload => {
            const shapes = updatePayload.updates.map(update => update.shapeID);
            updateEvent(`Shape Updated: ${shapes}`);
            logPayload(updatePayload)
        })

        remoteInterface.onShapeDeleted(shapePayload => {
            // 2.1.7
            if(!shapePayload.shapeIDs) {
                updateEvent(`Shape Deleted: ${shapePayload.shapeID}`);    
                logPayload(shapePayload)
                return;
            }
            // 2.1.8
            updateEvent(`Shape Deleted: ${shapePayload.shapeIDs}`);
            logPayload(shapePayload)
        });

        remoteInterface.onShapeSelected(shapeSelectedPayload => {
            updateEvent(`Shape Selected: ${shapeSelectedPayload.shapeIDs}`);
            logPayload(shapeSelectedPayload)
        });

        remoteInterface.onDisconnection(disconnectPayload => {
            if (instanceID && disconnectPayload.instanceID === instanceID) {
                updateInstanceID('');
                updateEvent(`Instance Disconnected: ${disconnectPayload.instanceID}`);
                logPayload(disconnectPayload)
            }
        });

        remoteInterface.onSystemMessage((messagePayload) => window.alert(`System Broadcast\n${messagePayload.message}`))
        remoteInterface.onSystemDisconnect(() => {
            updateEvent('Disconnected by server');
        })
    });
}

// Action Functions //

document.getElementById('viewport').addEventListener('click',viewportImage)

function viewportImage() {
    updateEvent('Requesting viewport image...')
    remoteInterface.requestViewportImage().then(p => {
        updateEvent('Received image: ' + p.url);
        logPayload(p);
        console.log('Request Viewport Image Promise', p);
        window.open(p.url);
    })
}

document.getElementById('getShape').addEventListener('click',getShapes)

window.getShapes = getShapes

function getShapes() {
    updateEvent('Getting shapes...');
    remoteInterface
        .getShapes({
            instanceID: instanceID
        })
        .then(r => {
            updateEvent('Retrieved Shapes');
            logPayload(r);
            console.log('Retrieved Shapes: ', r);
            window.shapes = r
        });
}

document.getElementById('connect').addEventListener('click',autoConnect)

function autoConnect() {
    updateEvent('Demonstrating auto connect... (check example.js::autoConnect)');
    remoteInterface.disconnect(); // In case we are already connected
    remoteInterface.autoConnect(CONFIG.messageBroker, 'WEBSOCKETS').then(statusPayload => {
        updateInstanceID(statusPayload.instanceID);
        //updateInstanceVersion(statusPayload.iSpyVersion);
        updateImage(
            statusPayload.imageID,
            statusPayload.imageSecurity,
            statusPayload.imageExtents
        );
        updateViewport(statusPayload.viewportExtents);
        updateEvent(`Auto Connected: ${statusPayload.instanceID}`);
        logPayload(statusPayload);
    })
}

document.getElementById('disconnect').addEventListener('click',disconnect)

function disconnect() {
    updateEvent('Disconnected');
    remoteInterface.disconnect();
}

function updateUser(user) {
    document.getElementById('connected_user_info').innerText = user;
    ispy_user = user
}

function updateGroup(group) {
    if(group) {
        ispy_group = group
        document.getElementById('group_info').innerText = group;
    } else {
        document.getElementById('group_info').innerText = "Set group using ?remote_group query parameter";
    }
}


function updateVersion(version) {
    document.getElementById('version').innerText = version;
}

function updateInstanceID(id) {
    if (!instanceID || id !== instanceID) {
        instanceID = id;
        document.getElementById('connected_instance').value = instanceID;
    }
}

function updateInstanceVersion(version) {
    document.getElementById('instance_version').innerText = version;
}

function updateImage(id, guide,  security, imageExtents, extra) {
    console.log(id)
    if(id) {
        document.getElementById('image_id').value = id;
        console.log(id)
    }
    if(extra){
        document.getElementById('shapes').innerText = extra;
    }
    if(guide) {
        document.getElementById('image_guide').innerText = guide;
    }
    if(security) {
        securityLabel = [security.classification, security.sciControl, security.dissemination].filter(label => !!label).join('//');
        document.getElementById('image_classification').value = securityLabel;
    }
    if(imageExtents) {
        const image_extents_ul = document.getElementById('image_extents_ul');
        const image_extents_ur = document.getElementById('image_extents_ur');
        const image_extents_ll = document.getElementById('image_extents_ll');
        const image_extents_lr = document.getElementById('image_extents_lr');
        image_extents_ul.value = formatLatLon(imageExtents.upperLeft);
        image_extents_ur.value = formatLatLon(imageExtents.upperRight);
        image_extents_ll.value = formatLatLon(imageExtents.bottomLeft);
        image_extents_lr.value = formatLatLon(imageExtents.bottomRight);
    }
}

function updateViewport(groundExtents) {
    if (!groundExtents) {
        return;
    }
    const viewport_extents_ul = document.getElementById('viewport_extents_ul');
    const viewport_extents_ur = document.getElementById('viewport_extents_ur');
    const viewport_extents_ll = document.getElementById('viewport_extents_ll');
    const viewport_extents_lr = document.getElementById('viewport_extents_lr');
    viewport_extents_ul.value = formatLatLon(groundExtents.upperLeft);
    viewport_extents_ur.value = formatLatLon(groundExtents.upperRight);
    viewport_extents_ll.value = formatLatLon(groundExtents.bottomLeft);
    viewport_extents_lr.value = formatLatLon(groundExtents.bottomRight);
}

function updateEvent(text) {
    const d = new Date();
    events.push(`[${d.toLocaleTimeString('en-US', { hour12: false })}] ${text}`);
    const textArea = document.getElementById('iSpyOutput');
    textArea.value = events
        .slice()
        .reverse()
        .join('\n');
}

function getUser() {
    // Get GeoAxis User in order to connect to User's iSpy
    updateEvent('Getting user...');
    return fetch(CONFIG.auth)
        .then(r => r.json())
        .then(auth => updateUser(parseUser(auth)))
        .catch(e => updateUser('No User'));
}

function parseUser(auth) {
    if (!auth) {
        return 'No User';
    }
    const userString = auth.dn;
    const cn = userString.indexOf('cn=');
    const com = userString.indexOf(',');
    if (cn === -1 || com === -1) {
        return 'No User';
    }
    const parsed = userString.slice(cn + 3, com);
    return parsed;
}

function formatLatLon(gc) {
    return `${gc.lat.toFixed(3)}, ${gc.lon.toFixed(3)}`;
}

function formatLineSample(ic) {
    return `${ic.line}, ${ic.sample}`;
}

function alignImage(e) {
    const img = document.getElementById('drag_image');
    e.dataTransfer.setDragImage(img, img.width / 2, img.height / 2);
}

function logPayload(p) {
    const textarea = document.getElementById('iSpyOutput');
    textarea.value = JSON.stringify(p, null, '\t');
    
}

//console.log(data['shapes'][0]['geometry']['coordinates'])