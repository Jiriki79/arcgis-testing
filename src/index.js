import esriConfig from "@arcgis/core/config";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer";
import Graphic from "@arcgis/core/Graphic";
import * as geometryEngine from "@arcgis/core/geometry/geometryEngine";
import FeatureTable from '@arcgis/core/widgets/FeatureTable'
import Editor from "@arcgis/core/widgets/Editor"
import Query from "@arcgis/core/rest/support/Query.js";
import IdentityManager from "@arcgis/core/identity/IdentityManager.js";
import OAuthInfo from "@arcgis/core/identity/OAuthInfo.js";
import Portal from "@arcgis/core/portal/Portal.js";
import PortalGroup from "@arcgis/core/portal/PortalGroup.js";
import PortalItem from "@arcgis/core/portal/PortalItem.js";

import { ispy_user, image_id, instanceID, ispy_group, securityLabel, shapes } from "./ipsy_functions";

var point
esriConfig.apiKey = "AAPK4cf070f48d9447b2af48df14234fb1c1iQX7jgpsU_O_g6eHZ7JOvND6f5_H5aiAembWWExxH_7g1-pPgwzqDxd8MJe2N5JZ";
const info = new OAuthInfo({
  appId: "7qy58CyijQNNIxDq", // Replace with your app id+
  flowType: "implicit",
  popup: true
});

IdentityManager.registerOAuthInfos([info]);

IdentityManager.getCredential(info.portalUrl + "/sharing");
IdentityManager.checkSignInStatus(info.portalUrl + "/sharing")
  .then((info) => {
    console.log("User is signed in as: ", info.userId);
    window.username = info.userId
  })
  .catch(() => {
    console.log("User is not signed in.");
  });

/*
// Load the item
item.load().then(() => {
  console.log(item.title); // Print the item's title
});
*/
esriConfig.assetsPath = "./assets";

const map = new Map({
  basemap: "arcgis-nova"
});
const bufferLayer = new GraphicsLayer();
const graphicsLayer = new GraphicsLayer();
const view = new MapView({
  map: map,
  center: [-97.305923,37.679377],
  zoom: 9,
  container: "viewDiv"
});

const Content = {
  "title": "API Test",
  "content": "<b>ID:</b> {ObjectID}<br><b>Location:</b> {place_name}<br><b>Image ID:</b> {imageID}<br><b>instance:</b> {instanceID}<br><b>Classification:</b> {classification}<br>"
}

const MyLayer = new FeatureLayer({
  url: "https://services8.arcgis.com/TrLhDuWxkcSkFAjt/arcgis/rest/services/api_test/FeatureServer/0",
  outFields:['*'],
  popupTemplate: Content
});

map.add(bufferLayer);
map.add(graphicsLayer);
map.add(MyLayer);

var option = 0
document.getElementById('bufferBtn').addEventListener("click", submit)
function submit(){
  const e = document.getElementById('dropdown');
  var option = e.value;
  const sym = {
    type: "simple-fill",
    color: [227, 139, 79, 0.25],
    style: "solid",
    outline: {
      color: [255, 255, 255, 255],
      width: 1
    }
  };
  
  MyLayer.queryFeatures().then((results)=>{
    graphicsLayer.removeAll();
      results.features.forEach((feat) => {
          const point = feat.geometry
          const buffer = geometryEngine.geodesicBuffer(point, option, "nautical-miles");
          var bufferGraphic = new Graphic({geometry: buffer, symbol: sym});
          // add graphic to map
          graphicsLayer.add(bufferGraphic);
        });
      })
};

const featureTable = new FeatureTable({
  view:view,
  autoRefreshEnabled:true,
  layer:MyLayer,
  container:"tableDiv",
  editingEnabled:true,
  hiddenFields:['OBJECTID']  
})

document.getElementById('clearBtn').addEventListener("click", ()=>{graphicsLayer.removeAll()});

const editor = new Editor({
  view:view
})

const checkbox = document.getElementById('checkboxId')

checkbox.onchange=()=>{toggle()}

function toggle(){
  if (checkbox.checked){
    view.ui.add(editor, 'top-right')
  } else {
    view.ui.remove(editor)
  }
}

const bufferCkbk = document.getElementById('bufferCkbk')

view.on('click',(event)=>{
  if (bufferCkbk.checked){
    point = event.mapPoint;
    const e = document.getElementById('dropdown');
    var option = e.value;
    const sym = {
      type: "simple-fill",
      color: [227, 139, 79, 0.25],
      style: "solid",
      outline: {
        color: [255, 255, 255, 255],
        width: 1
      }
    };
    
    MyLayer.queryFeatures().then((results)=>{
      results.features.map(feat => {
        const buffer = geometryEngine.geodesicBuffer(point, option, "nautical-miles");
        graphicsLayer.removeAll()
        var bufferGraphic = new Graphic({geometry: buffer, symbol: sym});
        // add graphic to map
        graphicsLayer.add(bufferGraphic);
      });
    })
  }
})

document.getElementById('connected_instance').value = instanceID

document.getElementById('postButt').addEventListener('click', function(){
  //id,user,classification, shape;
  coansole.log(ispy_user+"\n"+instnceID+"\n"+securityLabel+"\n"+image_id+"\n"+shapes)
  var [lon, lat] = window.shapes['shapes'][0]['geometry']['coordinates'].slice(0,2);
  //alert("lat:" + lat + "\nlon:"+ lon)
  var point = {
    type: "point",
    longitude: lon,
    latitude: lat
  };
  var pointGraphics = new Graphic({
    geometry: point,
    symbol:{
      type:'simple-marker',
      color: "blue",
      size: '16px'
    },
    attributes:{
      instanceID : instanceID,
      imageID: image_id,
      classification: securityLabel,
      username: window.username
    }
  });
  view.graphics.add(pointGraphics)
  MyLayer.applyEdits({
    addFeatures:[pointGraphics]
  }).then(function(editResult){
    if(editResult.addFeatureResults.length>0&&!editResult.addFeatureResults[0].error){
      console.log("Feature added successfully: ", editResult.addFeatureResults);
    } else {
      console.error("Failed to add feature: ", editResult.addFeatureResults[0].error);}
    })
});



